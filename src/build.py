from pathlib import Path

import os
import json
import shutil
import subprocess

class JobError(Exception):
    pass

class Target:
    def __init__(self, jobConfig):
        self.name = jobConfig['name']
        self.type = jobConfig['type']
        self.sources = [SourceFile(Path(source)) for source in jobConfig['sources']]
        self.headerSearchDirs = [Path(directory) for directory in jobConfig['header_search_dirs']]
        self.linkTargets = [targetName for targetName in jobConfig['link_targets']]
        self.compilerFlags = jobConfig['compiler_flags']

        self.outputDirectory = Path('build')

        self.objectFiles = [self.outputDirectory / source.path.parent / Path(source.path.name).with_suffix('.o') for source in self.sources]
        
        self.outputDirectory.mkdir(exist_ok=True)

    def compile(self):
        print('Compiling {}...'.format(self.name))
        output = subprocess.run(self.__generate_compile_command(), stderr=subprocess.PIPE, encoding='utf-8')

        compileOutputFile = self.outputDirectory / Path('{}_compile_output.txt'.format(self.name))
        compileOutputFile.touch()
        compileOutputFile.write_text(output.stderr)

        for objFile in self.objectFiles:
            objFile.parent.mkdir(exist_ok=True)
            shutil.move(Path(objFile.name), objFile)

    def link(self, targetMap):
        print('Linking {}...'.format(self.name))
        output = subprocess.run(self.__generate_linker_command(targetMap), stderr=subprocess.PIPE, encoding='utf-8')

        linkerOutputFile = self.outputDirectory / Path('{}_linker_output.txt'.format(self.name))
        linkerOutputFile.touch()
        linkerOutputFile.write_text(output.stderr)

        executable = Path(self.name + '.exe')
        executable.touch()
        shutil.move(executable, self.outputDirectory / executable)

    def __generate_compile_command(self):        
        sources = ' {} '.format(' '.join([str(source.path) for source in self.sources]))
        if self.headerSearchDirs:
            includeDirs = '-I {} '.format(' '.join([str(path) for path in self.headerSearchDirs]))
        else:
            includeDirs = ''
        
        compilerFlags = '-' + ' -'.join(self.compilerFlags) + ' '
        compileCommand = 'g++ -c ' + compilerFlags + includeDirs + sources
        return compileCommand

    def __generate_linker_command(self, targetMap):
        outputFile = '{}.exe '.format(self.name)
        objectFiles = ' '.join([str(objFile) for objFile in self.objectFiles])

        for targetName in self.linkTargets:
            objectFiles += ' ' + ' '.join([str(objFile) for objFile in targetMap[targetName].objectFiles])

        linkerCommand = 'g++ -o ' + outputFile + objectFiles
        print(linkerCommand)
        return linkerCommand

class SourceFile:
    def __init__(self, path):
        self.path = path
        self.includedFiles = list_includes_in(self.path)

def list_includes_in(filePath):
    lines = filePath.read_text().split('\n')
    includes = []
    for line in lines:
        if line.startswith('#include'):
            included = line.split(' ')[1]
            if included[-1] == '"' and included[0] == '"':
                includePath = Path(included[1:-1])
                includes.append(includePath)
    return includes

def run_from_build_file(pathToBuildFile):
    if isinstance(pathToBuildFile, Path):
        buildConfig = json.loads(pathToBuildFile.read_text())
    elif isinstance(pathToBuildFile, str):
        buildConfig = json.loads(Path(pathToBuildFile).read_text())
    else:
        raise JobError
    
    targetMap = {targetConfig['name']: Target(targetConfig) for targetConfig in buildConfig['targets']}
    for target in targetMap.values():
        target.compile()
    for target in targetMap.values():
        if target.type == 'executable':
            target.link(targetMap)

    print("Finished!")
if __name__ == "__main__":
    try:
        run_from_build_file('test/build.json')
    except Exception as e:
        print(e)