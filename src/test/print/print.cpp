#include <iostream>

#include "print.hpp"

std::string printString(const std::string& str) {
    std::cout << str << std::endl;
}